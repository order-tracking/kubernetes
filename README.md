## Continuous Deployment with Kubernetes and Gitlab CI

![Kubernetes](https://www.devopsnexus.com/user/pages/03.consultancy-areas/01.containerization/_technologies/kubernetes_logo.png)

Here lives the deployment configuration files for oTrack clusters using kubernetes.

*Due to Google Cloud Free trial quota limits and the cost for 4 months, we created 2 Kubernetes clusters in separate accounts, one cluster for Production and another one for Staging.* 

Custers Configuration:

- **Production Cluster:**
    - Restaurants SPA App: `https://otrack.info` -  2 replicas
    - oTrack API: `https://api.otrack.info`-  2 replicas
    - Client SPA App: `https://me.otrack.info` -  2 replicas
    - Identity Server: `https://identity.otrack.info`-  1 replica

- **Staging Cluster:**
    - Restaurants SPA App: `https://staging.otrack.info` - 2 replicas
    - oTrack API: `https://api-staging.otrack.info` - 2 replicas
    - Client SPA App: `https://me-staging.otrack.info` -  2 replicas
    - Identity Server: `https://identity-staging.otrack.info`-  1 replica

Each cluster has a nginx load balancer with a pod in each node (server) for high availability. Kube-Lego is configured and will renew the certificates from let's encrypt before expiring.

**Zero downtime deployments**, when updating an app the cluster creates a new container with the new version and then stops an older container. If the new version contains errors, the older containers are still up so there is no downtime ever.

## CI Workflow with Continuous Deployment
Each Gitlab Repository push triggers the pipeline and consequently triggers a kubernetes deploy with a new image.

- Each repository have a **develop** branch which deploys to the **Staging** cluster.
- Each repository have a **master** branch which deploys to the **Production** cluster.

![Continuous Delivery vs Continuous Deployment](https://puppet.com/sites/default/files/2016-09/puppet_continuous_diagram.gif)

## Architecture
![Architecture](imgs/architecture.png)

## Staging Cluster - Server Minimum Requirements
- 3x g1-small: 1 vCpu shared + 1.7GB RAM
- Total: 2 vCpus  ~ 5.10 GB RAM

## Production Cluster - Server Minimum Requirements
- 3x g1-small: 1 vCpu shared + 1.7GB RAM
- Total: 2 vCpus  ~ 5.10 GB RAM

## Database - Server Minimum Requirements
- 1x SQL Postgres instances with 0.6GB ram and 10GB ssd disk for **Staging**
- 1x SQL Postgres instances with 0.6GB ram and 10GB ssd disk for **Production**

## Google Cloud Platform Services Used
- Container Engine Cluster [GCE](https://cloud.google.com/container-engine/)
- Cloud SQL [SQL](https://cloud.google.com/sql/)

## Developer Setup Requirements
- Google Cloud SDK (`gcloud` binary) [Google Cloud SDK](https://cloud.google.com/sdk/)
- Kubernetes Client (`kubectl` binary) [kubectl](https://kubernetes.io/docs/tasks/kubectl/install/) - Can be installed from the Google Cloud SDK


## Steps before setting the Clusters
- Create the required hardware in Google Cloud (GKE & Databases)
- Push the repos to Gitlab with the respective branches. 
- Cancel the first Pipeline run after the build stage (required to upload the image to the gitlab registry)
- Setup the required Gitlab CI Variables in the Pipeline CI Settings

## How to obtain GCP Credentials (JWT token)
- GCP > Api Manager > Credentials > Create credentials > Service Account Key > JSON
- **Choose project, container, vm, sql permissions!**
- Required one per google account. (in case of multiple accounts, ex: 1 account per cluster)

## 1: Gitlab CI Setup
Setup these pipeline **variables** in the Repository > Settings > CI/CD Pipelines:

- **Restaurants** Web App Repository:
    - **PRODUCTION_CLUSTER**: [cluster-name] --zone [zone] --project [project] `ex: otrack-api --zone europe-west1-d --project otrack-api`
    - **STAGING_CLUSTER**: [cluster-name] --zone [zone] --project [project] `ex: otrack-cluster --zone europe-west1-b --project otrack-166622`
    - **PRODUCTION_GOOGLE_KEY**: Production Cluster Credentials JWT Token
    - **STAGING_GOOGLE_KEY**: Staging Cluster Credentials JWT Token
- **Client** Web App Repository:
    - **ALL the Restaurants Repository Variables**
- **Api** Repository:
    - **ALL the Restaurants Repository Variables** and these:
    - **DB_INSTANCE_PRODUCTION** - instance name for production environment (ex: otrack-api:europe-west1:otrack-api-production)
    - **DB_INSTANCE_STAGING** - instance name for staging environment
    - **oTrack__Database__PostgresConnection** - DB connection string to migrate the db (should be the same for the two environments)
- **Identity** Repository:
    - **ALL the API Repository Variables** and these:
    - **otrackIdentityServer__PfxPassword** - PFX password
    - **oTrackIdentity__Database__PostgresConnection** - DB connection string to migrate the db (should be the same for the two environments)

## 2: Setup foreach Kubernetes Cluster:
- Login to the Google Cloud:
    - `gcloud auth application-default login`
- Select the right project:
    - `gcloud config set project otrack-166622`
- Select the correct Container Engine Cluster from the GCP:
    - `gcloud container clusters get-credentials otrack-cluster --zone europe-west1-b --project otrack-166622`
- Add a new secret to kubernetes cluster with the Gitlab Registry Login so the containers can pull the private images from gitlab:
    - `kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=GITLAB_USERNAME --docker-password=GITLAB_PASSWORD --docker-email=GITLAB_EMAIL`
- Add the configuration for the default backend:
    - `kubectl apply -f 01-default-backend.yml` 
- Add the configuration for the **Restaurants Web App**:
    - `kubectl apply -f 02-restaurants.yml`
- Add the configuration for the **Api**:
    - Add a new secret with the user and password of the database:
        - `kubectl create secret generic cloudsql-db-credentials --from-literal=username=otrack --from-literal=password=password`
    - Allow the clusters vm ip addresses in each DB instance. **important**
    - Replace the configuration of the DB access in the `03-api.yml`
    - `kubectl apply -f 03-api.yml`
- Add the configuration for the **identity server**:
    - Add a new secret with the pfx password:
        - `kubectl create secret generic identityserver-pfx-password --from-literal=password=mypass`
        - `kubectl apply -f 04-identityserver.yml` (Change the DB ip)
- Add the configuration for the **Client Web App**:
    - `kubectl apply -f 05-client.yml`
- Add the configuration for the ingress & load balancers:
    - `kubectl apply -f 06-ingress.yml`
- Promove the ephemeral ip from the `nginx-ingress-service` to a static ip:
    - `kubectl patch svc nginx-ingress-service -p '{"spec": {"loadBalancerIP": "104.199.11.52"}}'` - Replace with the correct ephemeral ip
    - or from the gcloud panel: `https://console.cloud.google.com/networking/addresses`
- Change the domain dns to point the correct ips and sub-domains.
- Add the configuration for the [Kube-Lego](https://github.com/jetstack/kube-lego) which generates the SSL certificates:
    - `kubectl apply -f 07-kube-lego.yml` 

## Caution
- When adding new nodes to the clusters, is necessary to allow these new vm ip's in each db instance!
- VM instances have ephemeral ips, stoping it will release the ip so proceed with caution!
- Enable `Google Cloud SQL` & `Google Cloud SQL API` in the `APIs & Services > Dashboard`

## Useful links
- [Kube Lego](https://github.com/jetstack/kube-lego) - Generates SSL certificates from lets encrypt
- [nginx ingress](https://github.com/kubernetes/ingress/tree/master/controllers/nginx) - Documentation
- [nginx ingress static ip](https://github.com/kubernetes/ingress/blob/master/examples/static-ip/nginx/README.md) - Static ip Documentation
- [Kubernetes and Google Cloud](https://cloud.google.com/sql/docs/postgres/connect-container-engine)

## Problems faced
- Had to use the `nginx ingress` instead of the default `gce ingress` because the google cloud platform assigns a dedicated load balancer for the gce ingress and currently it only supports one certificate per static ip address.
There is a quota limit of 1 static ip per free trial account, so creating a new load balancer was not possible (more expensive also). In the end the `nginx ingress` is better because it supports tcp/udp/websockets connections where the `gce ingress` currently only supports http(s).

- Api containers require a DB connection, first attempt was successful with sidecar containers tunneling the connection by using a Cloud SQL Proxy container. This approach resulted in high cpu resources used by the proxy containers, instead we replaced this method with a direct connection to the ip of the database instance. The downside of this approach is the need of synchronization of the allowed ips required to grant access to the db instances.

- PFX password with `$` had to be escaped from the kubernetes secret (using `\$`) and from the Gitlab CI variables (`$$`)

- Kubernetes version 1.7.6 reserves a portion of each node resources for internal components, this caused an insufficient cpu flag in the existing replicas. We had to add a new node to each cluster to fix the issue.
